let collection = [];
let tail = 0;
let head = 0;

// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(element) {
	collection[tail++] = element;
	return collection;
}

function dequeue() {
	delete collection[head];
		return collection[head];
}

function front() {
    return collection[0];
}

function size() {
   return collection.length;
}

function isEmpty(){
    if(collection.length > 0) {
        return false;
    }
    else {
        return true;
    }
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};